const AppDB = require('./appDB.js');

exports.isAttrPresent = function(attr) {
    if(typeof(attr) == undefined) {
        return false;
    }
    if(attr == null) {
        return false;
    }
    if(attr.length == 0) {
        return false;
    }

    return true;
};

exports.generateToken = function(length) {
    var token = '';
    var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for(var i = 0; i < length; i++) {
        token += charSet.charAt(Math.floor(Math.random() * charSet.length));
    }

    return token;
};

exports.getUniqueIDforDB = async function(table, length) {
    var newToken = this.generateToken(length);

    while(await AppDB.recordExists(table, {roomID: newToken}, true)) {
        newToken = this.generateToken(length);
    }
    return newToken;
}

exports.encrypt = function(string) {
    return Buffer.from(string).toString('base64');
}

exports.decrypt = function(string) {
    return Buffer.from(string).toString('ascii');
}