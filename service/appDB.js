// appDB.js : Server Side
// 
// appDB.js handles calls into the mongoDB instance
// This is all abstracted out to keep app.js from
// getting to ugly

//library used for connecting Node to mongoDB
const mongojs = require('mongojs');

//My db object
var db = mongojs('mongodb://database/ear-buds', ['rooms', 'users']);

// ************************************************
// recordExists
// ************************************************
// Description: returns boolean based on if a query
// with provided param(s) against a table returns one
// or more values
//
// params:
//  table: 
//      Data type: String
//      Description: db table to query against
//  params: 
//      Data type: Object
//      Description: param(s) expected to be found in mongoDB table
//  singleValue: 
//      Data type: Boolean
//      Description: If singleValue = true, return true if and
//                   only if one entry is retrieved from the query
// 
// return:
//      Data type: Boolean
//      Descprition: Based on if an entry with the params exists
//                   in the table
// 
// Note: This function is asynchronous and returns a Promise.
//       Set up this way to keep away from callback hell.
// ************************************************
exports.recordExists = function(table, params, singleValue) {
    return new Promise(resolve => {
        db[table].find(params, (err, docs) => {
            if(err) {
                resolve(false);
            } else {
                if(singleValue) {
                    resolve(docs.length == 1);
                } else {
                    resolve(docs.length > 0);
                }
            }
        });
    });
}

// ************************************************
// getRecords
// ************************************************
// Description: all URLs that include /private/ must
// have a valid session token (stored in mongoDB).
// This function validates that the end user's cookie
// session_token is equal to the token I have stored in 
// mongo.
//
// params:
//  table: 
//      Data type: String
//      Description: db table to query against
//  params: 
//      Data type: Object
//      Description: param(s) expected to be found in mongoDB table
//  singleValue: 
//      Data type: Boolean
//      Description: If singleValue = true, return only the first
//                   record retrieved.
// 
// return:
//      Data type: Object
//      Descprition: Record(s) retrieved based the param(s) given
// 
// 
// Note: This function is asynchronous and returns a Promise.
//       Set up this way to keep away from callback hell.
// ************************************************
exports.getRecords = function(table, params, singleValue) {
    return new Promise(resolve => {
        db[table].find(params, (err, docs) => {
            if(err) {
                resolve(undefined);
            } else {
                if(docs.length == 0) {
                    resolve(undefined);
                } else if(singleValue) {
                    resolve(docs[0]);
                } else {
                    resolve(docs);
                }
            }
        });
    });
}

//TODO
// ************************************************
// createRecord
// ************************************************
// Description: all URLs that include /private/ must
// have a valid session token (stored in mongoDB).
// This function validates that the end user's cookie
// session_token is equal to the token I have stored in 
// mongo.
//
// params:
//  table: 
//      Data type: String
//      Description: db table to query against
//  params: 
//      Data type: Object
//      Description: param(s) expected to be found in mongoDB table
// 
// return:
//      Data type: Object
//      Descprition: Record(s) retrieved based the param(s) given
// 
// 
// Note: This function is asynchronous and returns a Promise.
//       Set up this way to keep away from callback hell.
// ************************************************
exports.createRecord = function(table, params) {
    return new Promise(resolve => {
        //Attempt to create table
        db[table].insert(params, (err, docs) => {
            if(err) {
                var toReturn = docs;
                toReturn.error = true;
                resolve(toReturn);
            } else {
                resolve(docs);
            }
        });
    });
}

// ************************************************
// modifyRecord
// ************************************************
//TODO FIX
// Describe
//
// params:
//  table: 
//      Data type: String
//      Description: db table to query against
//  query: 
//      Data type: Object
//      Description: param(s) used to query for DB entry
//  params: 
//      Data type: Object
//      Description: param(s) expected to be found in mongoDB table
// 
// return:
//      Data type: Object
//      Descprition: Record(s) retrieved based the param(s) given
// 
// 
// Note: This function is asynchronous and returns a Promise.
//       Set up this way to keep away from callback hell.
// ************************************************
exports.modifyRecord = async function(table, query, params) {
    return new Promise(resolve => {
        db[table].update(query, params, (err, docs) => {
            if(err) {
                var toReturn = docs;
                toReturn.error = true;
                resolve(toReturn);
            } else {
                resolve(docs);
            }
        });
    });
}

// ************************************************
// createUser TODO
// ************************************************
//TODO FIX
// Description: all URLs that include /private/ must
// have a valid session token (stored in mongoDB).
// This function validates that the end user's cookie
// session_token is equal to the token I have stored in 
// mongo.
//
// params:
//  table: 
//      Data type: String
//      Description: db table to query against
//  params: 
//      Data type: Object
//      Description: param(s) expected to be found in mongoDB table
// 
// return:
//      Data type: Object
//      Descprition: Record(s) retrieved based the param(s) given
// 
// 
// Note: This function is asynchronous and returns a Promise.
//       Set up this way to keep away from callback hell.
// ************************************************
exports.createRecord = function(table, params) {
    return new Promise(resolve => {
        //Attempt to create table
        db[table].insert(params, (err, docs) => {
            if(err) {
                var toReturn = docs;
                toReturn.error = true;
                resolve(toReturn);
            } else {
                resolve(docs);
            }
        });
    });
}

// ************************************************
// function: isAuthenticated
//
// cookies: request cookies. This funciton parses out
// the session_token cookie
// 
// return type: boolean
// returns: weather or not the session_token is valid
// 
// description: all URLs that include /private/ must
// have a valid session token (stored in mongoDB).
// This function validates that the end user's cookie
// session_token is equal to the token I have stored in 
// mongo.
// 
// This function is asynchronous and returns a Promise.
// Set up this way to keep away from callback hell.
// ************************************************
exports.isAuthenticated = function(cookies) {
    return new Promise(resolve => {
        var token = cookies.session_token;
        if(typeof(token) != 'undefined') {
            db.users.find({token: token}, (err, docs) => {
                //if the token is valid
                if(docs.length == 1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        } else {
            resolve(false);
        }
    });
};