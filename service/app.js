//TODO 
// Add ping to clean out old WS connections
// 

'use strict';

const express = require('express');
const request = require('request');
const cors = require('cors');
const querystring = require('querystring');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongojs = require('mongojs');

const Greenlock = require('greenlock-express');
const WebSocket = require('ws');

const AppUtils = require('./appUtils.js');
const AppDB = require('./appDB.js');

var ws;

//===================================
// Express 
//===================================
require('dotenv').config()

const client_id = process.env.SPOTIFY_CLIENT_ID;
const client_secret = process.env.SPOTIFY_CLIENT_SECRET;
const redirect_uri = process.env.SPOTIFY_CALLBACK_URL;

var db = mongojs('mongodb://database/ear-buds', ['rooms', 'users']);

var access_token;
var refresh_token;

var app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(__dirname + '/public'))
   .use(cors())
   .use(cookieParser());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', async (req, res) => {
    if(typeof(req.cookies.session_token) != 'undefined') {
        if(await AppDB.isAuthenticated(req.cookies)) {
            res.redirect('/private/home');
        } else {
            res.render('index.ejs');
        }
    } else {
        res.render('index.ejs');
    }
});

app.get('/public/signup', (req, res) => {
    res.render('signup.ejs');
});

app.get('/private/login', (req, res) => {

    // your application requests authorization
    //var scope = 'user-read-private user-read-email user-modify-playback-state user-read-playback-state streaming';
    var scope = "streaming user-read-birthdate user-read-email user-read-private";
    res.redirect('https://accounts.spotify.com/authorize?' +
        querystring.stringify({
        response_type: 'code',
        client_id: client_id,
        scope: scope,
        redirect_uri: redirect_uri,
        //show_dialog: true
        }));
});

app.get('/callback', (req, res) => {
    var code = req.query.code;
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
            code: code,
            redirect_uri: redirect_uri,
            grant_type: 'authorization_code'
    },
        headers: {
            'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
    },
        json: true
    };

    request.post(authOptions, (err, resp, body) => {
        if (!err && resp.statusCode === 200) {

            access_token = body.access_token,
            refresh_token = body.refresh_token;

            var options = {
              url: 'https://api.spotify.com/v1/me',
              headers: { 'Authorization': 'Bearer ' + access_token },
              json: true
            };

            res.cookie('s_access', access_token);
            res.cookie('s_refresh', refresh_token);

            res.redirect('/private/home');
        };
    });


});

app.post('/public/auth', async (req, res) => {
    var loginId = req.body.username.toLowerCase();
    var auth = false;

    //okay, so you have authenticated. Now we need to associate you with a token to login
    //We will send back a token with valid auth. This will be sent with the /login request to log in
    var token;
    var record;

    //check if user entered username
    if(await AppDB.recordExists('users', {username_lower: loginId})) {
        record = await AppDB.getRecords('users', {username_lower: loginId}, true);
    } 
    //otherwise check if user used email
    else if(await AppDB.recordExists('users', {email: loginId})) {
        record = await AppDB.getRecords('users', {email: loginId}, true);
    }
    if (record != undefined) {
        auth = (record.password == AppUtils.encrypt(req.body.password));
        if(auth) {
            res.statusCode = 200;
            
            var token = AppUtils.generateToken(64);
            res.cookie('session_token', token);
            record.token = token;
            db.users.update({_id: record._id}, record);
            
            res.send(token);
        } else {
            res.sendStatus(401);
        }
    } 
    //otherwise I don't have them
    else {
        res.sendStatus(401);
    }
});

app.post('/public/usernameAvailable', async (req, res) => {
    var username = req.body.username;
    var exists = await AppDB.recordExists('users', {username_lower: username});

    res.send(!exists);
});

app.post('/public/emailAvailable', async (req, res) => {
    var email = req.body.email;
    var exists = await AppDB.recordExists('users', {email: email});

    res.send(!exists);
});

app.post('/public/createUser', (req, res) => {
    var newUser = req.body;

    //check if there is already a user with the same username
    db.users.find({username: newUser.username}, (err, docs) => {
        if(docs.length == 0) {
            //check if there is already a user with the same email
            db.users.find({email: newUser.email}, (err, docs) => {
                if(docs.length == 0) {
                    db.users.insert({
                        username: newUser.username, 
                        username_lower: newUser.username.toLowerCase(),
                        email: newUser.email.toLowerCase(), 
                        password: newUser.password});
                    res.sendStatus('201');
                }else {
                    //send back failure
                    res.sendStatus('400');
                }
            });
        }else {
            res.sendStatus('400');
        }
        //console.log(err);
    });    
});

app.get('/private/logout', async (req, res) => {
    if(await AppDB.isAuthenticated(req.cookies)) {
        res.clearCookie('session_token');
        res.statusCode = 200;
        res.send();
    } else {
        res.redirect('/?badsession=true');
    }
});

app.post('/private/rooms', async (req, res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    //parse roomname from body
    var roomname = req.body.room_name;
    var nameMatch = false;

    //check that roomname is defined
    if(!AppUtils.isAttrPresent(roomname)) {
        res.sendStatus(400);
        return;
    }
    //check that roomname is unique
    if(await AppDB.recordExists('rooms', {name: roomname})) {
        res.sendStatus(400);
        return;
    }

    //will be more dynamic in the future
    var currentUser = await AppDB.getRecords('users', {token: req.cookies.session_token}, true);
    var roomID = await AppUtils.getUniqueIDforDB('rooms', 16);
    var newRoom = {name: roomname, max_count: 100, current_count: 0, owner: currentUser.username, roomID: roomID};
    var response = await AppDB.createRecord('rooms', newRoom);
    if(response.error) {
        res.sendStatus(500);
    } else {
        res.sendStatus(201);
    }
});

app.get('/private/listen', (req, res) => {
    if(req.query.hasOwnProperty('r')) {
        res.render('player')
    } else {
        res.redirect('/private/home')
    }
});

app.get('/private/rooms', async (req, res) => {
    //Check if user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    //get all rooms
    var response = await AppDB.getRecords('rooms');
    var rooms = []
    
    for(var i = 0; i < response.length; i++) {
        rooms.push({
            name : response[i].name,
            current_count : response[i].current_count,
            max_count : response[i].max_count,
            owner : response[i].owner,
            roomID: response[i].roomID
        });
    }

    res.send(rooms);
});

app.get('/private/home', async (req,res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    res.render('home');
});

app.get('/private/player', async (req,res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    res.render('player');
});

app.get('/private/player/search', async (req, res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    if(req.query.q.length != 0) {
        var query = req.query.q;
        var options = {
            url: 'https://api.spotify.com/v1/search?' 
            + querystring.stringify({
                q: query,
                type: 'album,track,playlist',
                limit: 5 
            }),
            headers: {'Authorization': 'Bearer ' + req.cookies.s_access},
        }

        request.get(options, (err, resp, body) => {
            if(err) {
                res.sendStatus(500);
            } else {
                res.send(body);
            }
        });
    } else {
        res.sendStatus(400);
    }
});

app.get('/private/player/current', async (req, res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    var device;

    var options = {
        url: 'https://api.spotify.com/v1/me/player/devices',
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    request.get(options, function(err, resp, body) {
        for(var i = 0; i < body.devices.length; i++) {
            if(body.devices[i].is_active) {
                device = 'Connected to: ' + body.devices[i].name + '. Type: ' + body.devices[i].type;
                break;
            }
        }

        res.send(device);
    });
});

//TODO actually use room ID
app.post('/private/player/event', async (req, res) => {
    //Check user is authenticated
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    sendPlayerEvent(req.body);
    updateDatabaseStatus(req.body);
    res.sendStatus(200);
});

app.get('/private/player/state', async (req,res) => {
    if(!await AppDB.isAuthenticated(req.cookies)) {
        res.redirect('/?badsession=true');
        return;
    }

    console.log(req.body);
    var roomRecord = await AppDB.getRecords('rooms', {roomID: req.body.roomID}, true);
    console.log(roomRecord);
    var playerState = roomRecord.state;

    res.send(playerState);
});

app.get('/private/player/status', (req,res) => {
    var options = {
        url: 'https://api.spotify.com/v1/me/player/currently-playing',
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    // use the access token to access the Spotify Web API
    request.get(options, function(err, resp, body) {
        if(typeof(body.item) != 'undefined')
        {
            var currentData = {
                progress: body.progress_ms,
                duration: body.item.duration_ms,
                playing: body.is_playing,
                songName: body.item.name
            };

            res.send(currentData);
        } else {
            res.sendStatus(400);
        }
    });
});

//===================================
// Greenlock
//===================================

var greenlock = Greenlock.create({
    // Let's Encrypt v2 is ACME draft 11
    version: 'draft-11'
    // Note: If at first you don't succeed, switch to staging to debug
    // https://acme-staging-v02.api.letsencrypt.org/directory
  , server: 'https://acme-v02.api.letsencrypt.org/directory'
    // Where the certs will be saved, MUST have write access
  , configDir: './.config/acme/'
    // You MUST change this to a valid email address
  , email: process.env.GREENLOCK_EMAIL
    // You MUST change these to valid domains
    // NOTE: all domains will validated and listed on the certificate
  , approveDomains: [ process.env.GREENLOCK_URL, 'www.'+process.env.GREENLOCK_URL ]
    // You MUST NOT build clients that accept the ToS without asking the user
  , agreeTos: true
  , app: app
    // Join the community to get notified of important updates
  , communityMember: false
    // Contribute telemetry data to the project
  , telemetry: true
  //, debug: true
  });
  
  var server = greenlock.listen(80, 443);

//===================================
// Web Socket
//===================================
ws = new WebSocket.Server({server: server});

var clients = {};
ws.on('connection', (conn, req) => {
    if(req.url.length < 16) {
        console.log('BAD ROOM ID!');
        return;
    }
    console.log(req.connection.remoteAddress + ' has connected');
    var roomID = req.url.slice(2)
    if(!clients.hasOwnProperty(roomID)) {
        clients[roomID] = [];
    }

    conn.roomID = roomID;
    clients[roomID].push(conn);
    console.log(roomID + ' now has ' + clients[roomID].length + ' users');

    conn.on('close', () => {
        var user = connectionSearch(clients, conn);
        if(user == undefined) {
            return;
        }

        clients[user.roomID].splice(clients[user.roomID].indexOf(user), 1);
        console.log("connection closing in room : " + user.roomID + ".");
        if(clients[roomID].length == 0) {
            console.log("No active sessions in room : " + user.roomID + ". Pausing player.");
            updateDatabaseStatus({roomID: roomID, paused: true});
        }
    });

    conn.on('message', (data) => {
        var json = JSON.stringify({type:'message', data: data});
        console.log(json);
        for (var i=0; i < clients[conn.roomID].length; i++) {
            try{
                if(clients[conn.roomID][i].readyState != 3) {
                    console.log('RS: ' + clients[conn.roomID][i].readyState);
                    clients[conn.roomID][i].send(json);
                }
            }
            catch(e) {
                console.log(e);
            }
        }
    });
});

//===================================
// UTILS
//===================================
var validateIdentity = function(token, callback) {
    db.users.find({token: token}, (err, docs) => {
        //if the token is valid
        if(docs.length == 1) {
            callback(false, docs[0]);
        } else {
            console.log("bad session");
            callback(true, docs);
        }
    });
}

var sendPlayerEvent = function(state) {
    var json = state;
    json.type = 'player-event';
        for (var i=0; i < clients[json.roomID].length; i++) {
            if(clients[json.roomID][i].readyState != 3) {
                clients[json.roomID][i].send(JSON.stringify(json));
            }
        }
}

var updateDatabaseStatus = async function(state) {
    console.log(state);
    var currentState = await AppDB.getRecords('rooms', {roomID: state.roomID}, true);
    if(!currentState.hasOwnProperty('state')) {
        currentState.state = {};
        currentState.state.position = 0;
    }

    var newState = currentState.state;
    for(var i in state) {
        newState[i] = state[i];
    }
    AppDB.modifyRecord('rooms', {roomID: state.roomID}, {$set: {state: newState}});
}

var connectionSearch = function(clients, conn) {
    if(!clients.hasOwnProperty(conn.roomID)) {
        return undefined;
    }

    for(var user in clients[conn.roomID]) {
        if(clients[conn.roomID][user] === conn) {
            return conn;
        }
    }

    return undefined;
}