// Main.js is a page to be filled with helper functions
// that many pages will need. All html (or ejs) pages
// should probably include this script file

var onEnter = () => {};

// ************************************************
// function: sendRequest
//
// method: HTML Method. Ex. GET, POST, PUT...
// url: URL to send request to
// data: payload if using methods such as POST 
// 
// return type: Promise
// returns: response for request
// 
// description: function that uses xhr to make requests
// As requests should be asynchronous this method returns
// a promise. Expectation is to call this function with
// 'await' from inside an async function
// ************************************************
var sendRequest = function(method, url, data) {

    // Promise is used for an async call that will
    // be waiting on the node server
    return new Promise(resolve => {

        // request will be an xhr
        var req = new XMLHttpRequest();

        // Add event listener for readystate changing to 'done'
        req.onreadystatechange = function() {
            
            // 4 means the request is done
            if (this.readyState == 4) {
                
                // I am simply sending the whole response back to
                // the function that called sendRequest()
                resolve(req);
            }
        };

        // after opening request, json is forced for the time being
        // I may add an argument to set different content in
        // the future
        req.open(method, url, true);
        req.setRequestHeader("Content-Type", "application/json");
        
        // As this function is used for HTTP methods with and
        // without payloads, I only send the 'data' attribute
        // if it is defined. Otherwise, send no payload
        if(data != undefined) {
            req.send(JSON.stringify(data));
        } else {
            req.send();
        }
    });    
}

// ************************************************
// function: getCookieByKey
//
// keyVal: desired cookie's key (or name)
// 
// return type: string
// returns: cookie value associated with given key value
// 
// description: cookies are returned in vanilla js
// as a string delemeted like so:
// "key1=val1; key2=val2; key3=val3; ..."
// 
// This function parses the document cookie string
// and will return the value of a cookie associated
// with the keyVal provided.
// ************************************************
var getCookieByKey = function (keyVal) {
    var cookies = document.cookie.split('; ');
    for(var i = 0; i < cookies.length; i++) {
        if(cookies[i].split('=')[0] == keyVal) {
            return cookies[i].split('=')[1];
        }
    }

    return undefined;
};

var getQueryValueByKey = function(key) {
    var href = window.location.href;
    var queryStrings = '';
    var toReturn = '';

    if(href.indexOf('?') > 0) {
        queryStrings = href.split('?')[1];
        keyValPairs = queryStrings.split('&')
        for(var i = 0; i < keyValPairs.length; i++) {
            if(keyValPairs[i].split('=')[0] == key) {
                toReturn = keyValPairs[i].split('=')[1];
                break;
            }
        }
    }

    return toReturn
}
