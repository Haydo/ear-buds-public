// Javascript for the root/homepage of Earbuds

// I always put my functions relying on document elements inside window.onload
// This ensures that the elements exist before modifying them in any way

window.onload = function() {

    // immediately when the page is ready, GET the
    // most current room list from server-side
    refreshRoomList();

    // When clicking the refresh button, just
    // call another refresh
    this.document.getElementById("room-refresh-btn").onclick = function() {
        refreshRoomList();
    };

    // On logout, instead of just going back to '/'
    // I make a GET to the server in order to let
    // the server remove the active session_token
    // from the logging out user.
    // async because of server-side communcation
    this.document.getElementById("log-out-btn").onclick = async function() {

        // sendRequest() is located in main.js
        var res = await sendRequest("GET", '/private/logout');
        if(res.status == 200) {
            window.location.href = '/';
        }
    };

    // creating a room posts a proposed room name
    // to the server. If it is available, the room
    // will be created. Rooms must have unique names.
    // async for server-side communication
    this.document.getElementById("create-room-btn").onclick = async function() {

        // store proposed room name in a var for easy access
        var roomName = document.getElementById('new-room-name').value;

        // only attempt a create if something is
        // actually in the room input field
        if(roomName != '') {

            // make POST with a payload containing 'room_name'
            var res = await sendRequest("POST", '/private/rooms', 
                {'room_name': roomName}
            );

            // if it looks like everything went well,
            // refresh the room list and the room should
            // show up.
            if(res.status == 201) {
                refreshRoomList();
            }
            // if the request response with a 403 that means
            // the user making the call has a bad session_token
            // Will add better error messaging later, but for
            // now it simply sends the user back to the login page.
            else if(res.status == 403) {
                window.location.href = '/?badsession=true';
            }
            // last case for now is probably a 500 due to name being taken
            // this can be more fine-grained later on
            else {
                alert('Failed to create room. Make sure the name is unique.');
            }
        } else {

            // yell if no room name given. Need to make
            // less ugly and more useful
            alert("Please enter in a room name.");
        }       
    };
}

// ************************************************
// function: refreshRoomList
// 
// return type: void
// 
// description: because loading, refreshing, and creating
// on this page causes a refresh of rooms, I threw the 
// login into a function. Simply asynchronously GETs the 
// room list from server-side.
// Sends off an array of room data objects to the regenerateTable()
// function to actually render. The room data returned
// is a subset of all room data stored on the backend DB
// ************************************************
var refreshRoomList = async function() {

    // sendRequest() is located in main.js
    var res = await sendRequest("GET", '/private/rooms');

    // if the rooms are returned successfully (200), then
    // send the response off to regenerateTable() to
    // build the table for the user.
    if(res.status == 200) {
        regenerateTable(JSON.parse(res.response));

    // if the request respons with a 403 that means
    // the user making the call has a bad session_token
    // Will add better error messaging later, but for
    // now it simply sends the user back to the login page.
    } else if(res.status == 403) {
        window.location.href = '/?badsession=true';
    }
}

// ************************************************
// function: regenerateTable
// 
// roomData: array of room objects containing info
// such as room name, creator, and users in the room
// 
// return type: void
// 
// description: renders a table of avaiable rooms
// based on room data. It is pretty hard-coded right
// now. I will probably make it a lot more abstract.
// ************************************************
var regenerateTable = function(roomData) {
    var roomTable = document.getElementById("room-table");

    //jeez. please change
    //roomTable.innerHTML = "<tr> <td>Room Name</td> <td>Owner</td> <td>Listeners</td> <td>Capacity</td> </tr>";
    roomTable.innerHTML = '';
    var headerRow = document.createElement('tr');
    headerRow.id = "table-header-row";
    headerRow.innerHTML = "<td>Room Name</td><td>Created By</td><td>Users</td><td>Capacity</td>";
    roomTable.appendChild(headerRow);

    for(var i = 0; i < roomData.length; i++) {
        // create a new row to put room data into
        var row = document.createElement('tr');
        row.id = roomData[i].roomID;
        row.onclick = function() {
            window.location.href = "/private/listen?r=" + this.id;
        }
        
        //create the name column and add room name
        var roomName = document.createElement('td');
        roomName.innerHTML = roomData[i].name;

        //create the owner column and add room owner
        var roomOwner = document.createElement('td');
        roomOwner.innerHTML = roomData[i].owner;

        //create the user count column and add count
        var roomCount = document.createElement('td');
        roomCount.innerHTML = roomData[i].current_count;

        //create the max users column and add max
        var roomCapacity = document.createElement('td');
        roomCapacity.innerHTML = roomData[i].max_count;

        //Add all elements to the row; Then add row to table
        row.appendChild(roomName);
        row.appendChild(roomOwner);
        row.appendChild(roomCount);
        row.appendChild(roomCapacity);

        roomTable.appendChild(row);
    }

};