var socket;

//TODO how not to do this?
var prevState = {
    position: 0,
    paused: true,
    track: {
        uri: 0,
        album: 0,
        artist: 0,
        name: 0,
        image_url: 0
    }
}

var userSeek = false;
var showPause = true;

window.onload = function() {
    openWebSocket(getQueryValueByKey('r'));
    syncToRoomState(getQueryValueByKey('r'));
    // Search for songs. Will be moved to new page after testing
    this.document.getElementById("search-btn").onclick = async function() {
        var searchValue = document.getElementById('search-input').value;
        if(searchValue.length != 0) {
            var res = await sendRequest('GET', '/private/player/search?q=' + searchValue);
            showSearchResults(JSON.parse(res.response));
        } else {
            alert('I won\'t search for nothin');
        }
    };

    //set up hotkeys
    setUpHotkeys();

    this.document.getElementById('back-btn').onclick = function() {
        window.location.href = "/private/home";
    }

    // Search for songs. Will be moved to new page after testing
    this.document.getElementById('toggle-btn').onclick = async function() {
        /*
        if(player) {
            var res = await sendRequest("GET", '/private/player/togglePlay');
        }
        */
        if(player) {
            showPause = !showPause;
            player.togglePlay();

            if(showPause) {
                document.getElementById('toggle-btn').style.backgroundImage = "url(../res/pause.svg)";
            } else {
                document.getElementById('toggle-btn').style.backgroundImage = "url(../res/play-button.svg)";
            }
        }
        
    };

    this.document.getElementById('vol-slider').oninput = function() {
        if(player) {
            player.setVolume((this.value)/100);
        }
    }

    //will be blank off the bat
    //updatePlayerInfo(req);
};

var player;
var device_id;
//var spotify_uri = "spotify:track:0aym2LBJBk9DAYuHHutrIl";
window.onSpotifyWebPlaybackSDKReady = () => {
    const token = getCookieByKey('s_access');
    player = new Spotify.Player({
        name: 'Buds',
        getOAuthToken: cb => { cb(token); }
    });

    // Error handling
    player.addListener('initialization_error', ({ message }) => { console.error(message); });
    player.addListener('authentication_error', ({ message }) => { console.error(message); });
    player.addListener('account_error', ({ message }) => { console.error(message); });
    player.addListener('playback_error', ({ message }) => { console.error(message); });

    // Playback status updates
    player.addListener('player_state_changed', (state) => {
        sendStateChange();
    });

    // Ready
    player.addListener('ready', ({ device_id }) => {
        this.device_id = device_id;
        console.log('Ready with Device ID', device_id);
    });

    // Not Ready
    player.addListener('not_ready', ({ device_id }) => {
        console.log('Device ID has gone offline', device_id);
    });

    // Connect to the player!
    player.connect();
};

var playTrack = async function(spotify_uri) {
    document.getElementById('toggle-btn').style.backgroundImage = "url(../res/pause.svg)";
    showPause = true;
    fetch('https://api.spotify.com/v1/me/player/play?device_id=' + device_id, {
            method: 'PUT',
            body: JSON.stringify({uris: [spotify_uri] }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookieByKey('s_access')
            },
        });
}

//TODO This data stuff should be on server side
var sendStateChange = async function() {
    var sendEvent = false;
   //console.log(JSON.stringify(state));
   var state = await player.getCurrentState();
    var roomState = {
        position: state.position,
        duration: state.duration,
        paused: state.paused,
        track: {
            uri: state.track_window.current_track.uri,
            album: state.track_window.current_track.album.name,
            artist: state.track_window.current_track.artists[0].name,
            name: state.track_window.current_track.name,
            image_url: state.track_window.current_track.album.images[0].url
        }
    }
    
    //TODO add seeking functionality
    if(prevState != undefined) {
        if(roomState.paused != prevState.paused) {
            sendEvent = true;
        }

        if(roomState.track.uri != prevState.track.uri ) {
            sendEvent = true;
        }

        
        if(userSeek) {
            userSeek = false;
            sendEvent = true;
        }
    } 

    roomState.roomID = getQueryValueByKey('r');

    prevState = roomState;
    if(sendEvent) {
        var res = await sendRequest('POST', '/private/player/event', roomState);
    };
        
        //var res = await sendRequest('POST', '/private/player/event', roomState);
        //TODO Error handling
};

var handleStateChange = async function(req) {
    var playState = await player.getCurrentState();

    //hanlde pause event
    if(req.paused != undefined) {
        if(req.paused) {
            player.pause();
        } else if (!req.paused) {
            player.resume();
        }
    }

    //handle track change event
    if(req.track != undefined) {
        if(playState.track_window.current_track.uri != req.track.uri) {
            fetch('https://api.spotify.com/v1/me/player/play?device_id=' + device_id, {
                method: 'PUT',
                body: JSON.stringify({uris: [req.track.uri] }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + getCookieByKey('s_access')
                },
            });
        } else {
            console.log("dropping double change")
        }

        updatePlayerInfo(req);
    }

    //handle seek event
    if(req.position != undefined) {
        console.log('seeking to: ' + req.position);
        player.seek(req.position);
    }
    /*
    var playerState = await player.getCurrentState();
    if(req.paused) {
        player.pause();
    } else if (!req.paused) {
        player.resume();
    }

    /*
    //change play/pause
    if(req.paused != playerState.paused) {
        console.log('play/pause change');
        if(req.paused) {
            player.pause();
            player.seek(req.position);
        } else {
            player.seek(req.position);
            player.resume();
        }
    }
    *//*
    //change track
    if(req.track.uri != playerState.track_window.current_track.uri) {
        console.log('track change');
        fetch('https://api.spotify.com/v1/me/player/play?device_id=' + device_id, {
            method: 'PUT',
            body: JSON.stringify({uris: [req.track.uri] }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookieByKey('s_access')
            },
        });
    }
    */
}

var openWebSocket = function(roomID) {
    socket = new WebSocket('wss://dev.haydenives.com?' + roomID);

    socket.onopen = function() {
        console.log("Socket connection successful");
    }

    socket.onmessage = function(msg) {
        var request = JSON.parse(msg.data);

        switch(request.type) {
            case 'message':
                alert('message recieved: ' + request.data);
                break;
            case 'player-event':
                handleStateChange(request);
        }
    };

};

//TODO add default for when nothing is there
var updatePlayerInfo = function(req) {
    if(document.getElementById('track-info-div')) {
        document.getElementById('player-div').removeChild(document.getElementById('track-info-div'));
        document.getElementById('player-div').removeChild(document.getElementById('track-img-div'));
    };

    var trackInfoDiv = document.createElement('div');
    trackInfoDiv.id = 'track-info-div';
    document.getElementById('player-div').appendChild(trackInfoDiv);

    var trackImgDiv = document.createElement('div');
    trackImgDiv.id = 'track-img-div';
    document.getElementById('player-div').appendChild(trackImgDiv);

    var albumImg = document.createElement('img');
    albumImg.id = 'current-track-img';
    albumImg.src = req.track.image_url;
    trackImgDiv.appendChild(albumImg);

    var songName = document.createElement('span');
    songName.id = 'song-name';
    songName.innerHTML = req.track.name;
    trackInfoDiv.appendChild(songName);

    var albumInfo = document.createElement('span');
    albumInfo.id = "album-info";
    albumInfo.innerHTML = req.track.artist + " - " + req.track.album;
    trackInfoDiv.appendChild(albumInfo);
}

var showSearchResults = function(res) {
    if(document.getElementById('search-div')) {
        document.getElementsByTagName('main')[0].removeChild(document.getElementById('search-div'));
    }

    var searchDiv = document.createElement('div');
    searchDiv.id = 'search-div';
    document.getElementsByTagName('main')[0].appendChild(searchDiv);

    var tracks = res.tracks.items;

    for(var i = 0; i < tracks.length; i++) {
        var itemDiv = document.createElement('div');
        itemDiv.setAttribute('uri', tracks[i].uri);
        itemDiv.onclick = function() {
            playTrack(this.getAttribute('uri'));
        }

        var img = document.createElement('img');
        img.src = tracks[i].album.images[2].url;
    
        var titleSpan = document.createElement('span');
        titleSpan.innerHTML = tracks[i].name;
    
        var artistSpan = document.createElement('span');
        artistSpan.innerHTML = tracks[i].artists[0].name;
    
        var albumSpan = document.createElement('span');
        albumSpan.innerHTML = tracks[i].album.name;
    
        var lineBreak = document.createElement('br');
    
        itemDiv.appendChild(img);
        itemDiv.appendChild(titleSpan);
        itemDiv.appendChild(artistSpan);
        itemDiv.appendChild(albumSpan);
        itemDiv.appendChild(lineBreak);

        searchDiv.appendChild(itemDiv);
    }
};

var syncToRoomState = async function(roomID) {
    var res = await sendRequest('GET', '/private/player/state', {'roomID': roomID});
    var state = JSON.parse(res.response);
    updatePlayerInfo(state);
}

var setUpHotkeys = function() {
    //if typing in an input field, don't pause on space
    //also, only send search if input is in focus
    var inputFocus = false;
    document.getElementById('search-input').onfocus = function() {
        inputFocus = true;
    }
    document.getElementById('search-input').onblur = function() {
        inputFocus = false;
    }

    var onEnter = () => {
        var btn = document.getElementById('search-btn');
        if(typeof(btn) != 'undefined' && inputFocus) {
            btn.click();
        }
    };

    var onSpaceBar = () => {
        var btn = document.getElementById('toggle-btn');
        if(typeof(btn) != 'undefined' && !inputFocus) {
            btn.click();
        }
    };

    var onUpArrow = () => {
        var volume = document.getElementById('vol-slider');
        volume.stepUp(5);
        volume.oninput();
    };
    var onDownArrow = () => {
        var volume = document.getElementById('vol-slider');
        volume.stepDown(5);
        volume.oninput();
    };

    var onLeftArrow = async () => {
        var state = await player.getCurrentState();
        userSeek = true;
        player.seek(state.position - 5000);
    };
    var onRightArrow = async () => {
        var state = await player.getCurrentState();
        userSeek = true;
        player.seek(state.position + 5000);
    };

    document.onkeydown = function(event) {
        switch(event.keyCode) {
            case 38:
                onUpArrow();
                break;
            case 40:
                onDownArrow();
                break;
            default:
                break;
        }
    }

    document.addEventListener('keyup', (event) => {
        switch(event.keyCode) {
            case 13:
                onEnter();
                break;
            case 32:
                onSpaceBar();
                break;
            case 37:
                onLeftArrow();
                break;
            case 39:
                onRightArrow();
                break;
            default:
                break;
        }
    });
}