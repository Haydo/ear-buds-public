// Javascript for the root/homepage of Earbuds


//global vars?
var overlay = false;
var validForm = false;

// I always put my functions relying on document elements inside window.onload
// This ensures that the elements exist before modifying them in any way
window.onload = function() {
    this.document.getElementById('start-sign-up-btn').onclick = function() {
        showSignUpPrompt();
    }

    this.document.getElementById('sign-up-exit-btn').onclick = function() {
        closeSignUpPrompt();
    }

    this.document.getElementById('start-log-in-btn').onclick = function() {
        showLogInPrompt();
    }

    this.document.getElementById('log-in-exit-btn').onclick = function() {
        closeLogInPrompt();
    }

    this.document.getElementById('check-un-btn').onclick = async function() {
        var available = await checkUsernameAvailability();
        console.log(available);
    }

    // Logging in is done in an async function
    // as it must wait for a response from
    // the node server.
    this.document.getElementById('log-in-btn').onclick = async function() {
        
        // store input elements as variables to
        // easily recall and lower document parses
        var usernameInput = document.getElementById('username-input');
        var passwordInput = document.getElementById('password-input');

        // check to ensure both fields have atleast
        // been filled out before attempting auth
        if(usernameInput.value == '' || passwordInput.value == ''){

            // I need better error messages
            alert('please enter a username and password')
        } else {
            
            // call sendRequest to attempt to authenticate
            // sendRequest() is located in: main.js
            var res = await sendRequest('POST', '/public/auth', {
                username: usernameInput.value,
                password: passwordInput.value
            });

            // Check that auth was successful with a 200. 
            // The important part here is that the successful auth
            // should have put a cookie (session_token) onto
            // the user's browser for progressing in the login process
            if(res.status == 200) {
                window.location.href = '/private/login';
            } else if(res.status == 401) {
                alert('Username or password is incorrect');
            }
        
        }
    }

    this.document.getElementById('submit-sign-up-btn').onclick = async function() {
        //First check that all fields have been filled out
        var signInDiv = document.getElementById('submit-sign-up-btn');
        var fields = signInDiv.getElementsByTagName('input');
        for(var i = 0; i < fields.length; i++) {
            console.log(fields[i].value);
            //check that all fields are filled out 
            if(fields[i].value.length == 0) {
                console.log(fields[i].id);
                alert('Please enter: ' + fields[i].placeholder);
                break;
            }
        }

        //check attributes
        validForm = (await checkUsernameAvailability() && await checkEmailAvailability() && checkPasswordsMatch());
        if(validForm) {
            var userInput = document.getElementById('username');
            var emailInput = document.getElementById('email');
            var passInput = document.getElementById('password');

            var res = await sendRequest('POST', '/public/createUser', 
            {username: userInput.value,
             email: emailInput.value,
             password: btoa(passInput.value)});
            
            if(res.status == 201) {
                console.log("going back to home page");
                window.location.href = "/";
            }
        }else {
            alert("Username or email is already in use");
        }
    }

    this.document.getElementById('password-confirm').onkeyup = function() {
        checkPasswordsMatch();  
    };
}

var showSignUpPrompt = function() {
    document.getElementById('overlay').style.display = 'flex';
    document.getElementById('sign-up-div').style.display = 'flex';

    onEnter = () => {
        var btn = document.getElementById('submit-sign-up-btn');
        if(typeof(btn) != 'undefined') {
            btn.click();
        }
    };
};

var closeSignUpPrompt = function() {
    document.getElementById('overlay').style.display = 'none';
    document.getElementById('sign-up-div').style.display = 'none';

    onEnter = () => {};
};

var showLogInPrompt = function() {
    document.getElementById('overlay').style.display = 'flex';
    document.getElementById('log-in-div').style.display = 'flex';

    onEnter = () => {
        var btn = document.getElementById('log-in-btn');
        if(typeof(btn) != 'undefined') {
            btn.click();
        }
    };
};

var closeLogInPrompt = function() {
    document.getElementById('overlay').style.display = 'none';
    document.getElementById('log-in-div').style.display = 'none';

    onEnter = () => {};
};

checkUsernameAvailability = async function() {
    var userInput = document.getElementById('username');

    if(userInput.value.length > 0) {
        var res = await sendRequest('POST', '/public/usernameAvailable', {username: userInput.value});
        var nameAvailable = (res.response == 'true');
        console.log('nameAvailable: ' + nameAvailable);
        return nameAvailable;
    }
};

checkEmailAvailability = async function() {
    var emailInput = document.getElementById('email');

    if(emailInput.value.length > 0) {
        var res = await sendRequest('POST', '/public/emailAvailable', {email: emailInput.value});
        var emailAvailable = (res.response == 'true');
        console.log('emailAvailable: ' + emailAvailable);
        return emailAvailable;
    }
};

checkPasswordsMatch = function() {
    var confirmedPass = (document.getElementById('password').value == document.getElementById('password-confirm').value);
    console.log(confirmedPass);
    return confirmedPass;
};

document.addEventListener('keyup', (event) => {
    switch(event.keyCode) {
        case 13:
            onEnter();
            break;
    }
});