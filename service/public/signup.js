//TODO Add more abstract db validation calls maybe
//bet checks
var validForm = false;

window.onload = function() {
    this.document.getElementById('submit-sign-up-btn').onclick = async function() {
        
        //First check that all fields have been filled out
        var fields = document.getElementsByTagName('input');
        for(var i = 0; i < fields.length; i++) {

            //check that all fields are filled out 
            if(fields[i].value.length == 0) {
                alert('Please enter: ' + fields[i].placeholder);
                break;
            }
        }

        //check attributes
        validForm = (await checkUsernameAvailability() && await checkEmailAvailability() && checkPasswordsMatch());
        if(validForm) {
            var userInput = document.getElementById('username');
            var emailInput = document.getElementById('email');
            var passInput = document.getElementById('password');

            var res = await sendRequest('POST', '/public/createUser', 
            {username: userInput.value,
             email: emailInput.value,
             password: btoa(passInput.value)});
            
            if(res.status == 201) {
                console.log("going back to home page");
                window.location.href = "/";
            }
        }else {
            alert("Username or email is already in use");
        }
    }

    /*
    document.getElementById('username').onblur = function() {
        checkUsernameAvailability();
    };

    document.getElementById('email').onblur = function() {
        checkEmailAvailability();
    };
    */

    document.getElementById('password-confirm').onkeyup = function() {
        checkPasswordsMatch();
    };
}

checkUsernameAvailability = async function() {
    var userInput = document.getElementById('username');

    if(userInput.value.length > 0) {
        var res = await sendRequest('POST', '/public/usernameAvailable', {username: userInput.value});
        var nameAvailable = (res.response == 'true');
        console.log('nameAvailable: ' + nameAvailable);
        return nameAvailable;
    }
}

checkEmailAvailability = async function() {
    var emailInput = document.getElementById('email');

    if(emailInput.value.length > 0) {
        var res = await sendRequest('POST', '/public/emailAvailable', {email: emailInput.value});
        var emailAvailable = (res.response == 'true');
        console.log('emailAvailable: ' + emailAvailable);
        return emailAvailable;
    }
}

checkPasswordsMatch = function() {
    var confirmedPass = (document.getElementById('password').value == document.getElementById('password-confirm').value);
    console.log(confirmedPass);
    return confirmedPass;
}