//TODO Make better README

This application allows you and friends to listen to Spotify (Premium needed) in sync.

### Installation

Ear Buds requires [Docker](https://www.docker.com/products/docker-desktop) to run.

To run, you first must configure a .env file. 

0. Set up an app in the Spotify dev portal
1. Create a file name .env in the /service folder.
2. Fill out the following settings:

- SPOTIFY_CALLBACK_URL - Configured in Spotify dev portal. App URL + /callback
- SPOTIFY_CLIENT_ID - Configured in Spotify dev portal
- SPOTIFY_CLIENT_SECRET - Configured in Spotify dev portal
- GREENLOCK_EMAIL - Email for automatic TLS setup
- GREENLOCK_URL - URL to secure (exclude "www.")


Example .env file contents:
```sh
SPOTIFY_CALLBACK_URL=buds.haydenives.com/callback
SPOTIFY_CLIENT_ID=abc123def456ghi789j0
SPOTIFY_CLIENT_SECRET=abc123def456ghi789j0
GREENLOCK_EMAIL=hayden@haydenives.com
GREENLOCK_URL=buds.haydenives.com
```

Your generated TLSCert will have SAN values with and without www.*
Example:
DNS Name=dev.haydenives.com
DNS Name=www.dev.haydenives.com

3. Once everything is configured within your .env file run:
```sh
./start.sh 
```
from the root directory
